# Pawprint FE Test Preparation


Requirements
---

**All operating systems:**   
The following entries must be added to your hosts file:
 ```
 127.0.0.1	paw.test
 127.0.0.1	api.paw.test
 ```

**Linux:**
 - Docker
 - Docker Compose
 - Git
 
**Mac:**
 - Docker Desktop for Mac
 - Git
 
**Windows: _(see note below)_**
 - WSL2 enabled and a distro installed
 - Docker Desktop for Windows 
 - Git for Windows

**Recommended Browser: Firefox**
 - We recommend using Firefox for this test
 - Chrome can have issues with setting the phpsessid needed for auth 

### Ensure nothing is using port 80
The Docker containers used for the test need to be bound to port 80 - if you have any other service (eg. a dev web server) using that port please ensure it is stopped before starting the test or you'll receive a 'Port is already allocated' error message when trying to start the test's Docker stack. 

### Log in to GitLab's container registry

To ensure you're able to download the Docker images used in the test please log in to GitLab's container registry after Docker has been installed:

`docker login registry.gitlab.com`

### Note for Windows Users:

WSL2 is now available in W10. You will need to have it installed and enabled to be able to sit this test properly; please see [here](https://docs.microsoft.com/en-us/windows/wsl/install-win10) for instructions. Recommended distros to use are [Ubuntu 20.04 LTS](https://www.microsoft.com/en-gb/p/ubuntu-2004-lts/9n6svws3rx71?rtc=1&activetab=pivot:overviewtab) or [openSUSE Leap 15.1](https://www.microsoft.com/en-gb/p/opensuse-leap-15-1/9njfzk00fgkv?rtc=1&activetab=pivot:overviewtab). 

Please also ensure Docker Desktop is using the WSL2 backend as detailed [here](https://docs.docker.com/docker-for-windows/wsl/).
